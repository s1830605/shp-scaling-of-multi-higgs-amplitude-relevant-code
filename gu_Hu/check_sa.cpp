#include <iostream>
#include <iomanip>

#include "CPPProcess.h"
#include "rambo.h"

int main(int argc, char** argv){

  // Create a process object
    CPPProcess process;

  // Read param_card and set parameters
    process.initProc("../../Cards/param_card.dat");

    double energy = 1500;
    double weight;

  // Get phase space point
  //  vector<double*> p = get_momenta(process.ninitial, energy,
  //                 process.getMasses(), weight);
  // Copy the phase space routine from ../../src/rambo.cc
  // Choose parameters, Delta will become part of for loop eventually
    
    for(double Delta=0; Delta<10;Delta=Delta+0.01)
    {
        double ptxtot(0.), ptytot(0.), pplus(0.),pminus(0.);
  //  Need to define (n-1) 'pt's, (n-1) 'phi's and (n-1) 'y's
        double pt1=40.;
        double phi1=3.14159/3.;
        double m=125.;
        double y1=-Delta;   //pa and p1 are negative direction
        double mperp = (sqrt(m*m + pt1*pt1));
        ptxtot+=pt1*cos(phi1);
        ptytot+=pt1*sin(phi1);
        pplus+=0.5*mperp*(cosh(y1)+sinh(y1));
        pminus+=0.5*mperp*(cosh(y1)-sinh(y1));
  //  Deduce 'n'th momentum by momentum conservation
        double ptn=sqrt(ptxtot*ptxtot+ptytot*ptytot);
        double yn=+Delta;
        pplus+=0.5*ptn*(cosh(yn)+sinh(yn));
        pminus+=0.5*ptn*(cosh(yn)-sinh(yn));
        double sij = 4*pplus*pminus;
       
        
  //  Use values above to find momenta

        vector<double*> p(1, new double[4]); //pa
        p[0][0] = pminus;
        p[0][1] = 0;
        p[0][2] = 0;
        p[0][3] = -pminus;
        p.push_back(new double[4]);  //pb
        p[1][0] = pplus;
        p[1][1] = 0;
        p[1][2] = 0;
        p[1][3] = pplus;
        p.push_back(new double[4]);  //p1, H
        p[2][0] = mperp*cosh(y1);
        p[2][1] = pt1*cos(phi1);
        p[2][2] = pt1*sin(phi1);
        p[2][3] = mperp*sinh(y1);
        p.push_back(new double[4]);  //p2
        p[3][0] = ptn*cosh(yn);
        p[3][1] = -ptxtot; // to balance px
        p[3][2] = -ptytot; // to balance py
        p[3][3] = ptn*sinh(yn);

   // cout << "p chosen" << p[0][0] << endl;
        
        
        if (p[0][0]+p[1][0]-p[2][0]-p[3][0] > 1.)
            {
                cout << "Error with momentum conservation!" << endl;
            }

  // Set momenta for this event
        process.setMomenta(p);

  // Evaluate matrix element
        process.sigmaKin();

        const double* matrix_elements = process.getMatrixElements();

 // cout << "Momenta:" << endl;
       // for(int i=0;i < process.nexternal; i++)
           // cout << setw(4) << i+1
           // << setiosflags(ios::scientific) << setw(14) << p[i][0]
           // << setiosflags(ios::scientific) << setw(14) << p[i][1]
          //  << setiosflags(ios::scientific) << setw(14) << p[i][2]
          //  << setiosflags(ios::scientific) << setw(14) << p[i][3] << endl;
 // cout << " -----------------------------------------------------------------------------" << endl;

  // Display matrix elements
    
//     << setiosflags(ios::fixed) << setprecision(17)
        for(int i=0; i<process.nprocesses;i++)
            {//cout << " Matrix element = "
  //     << setiosflags(ios::fixed) << setprecision(17)
                   // << matrix_elements[0]
                  //  << " GeV^" << -(2*process.nexternal-8) << endl;
                cout << Delta << "  " << matrix_elements[i]/(sij*sij)<< endl;
            }
    }
}
