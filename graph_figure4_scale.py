#import relevant libraries
import pylab
import numpy as np
import matplotlib.pyplot as plt
import csv

# creates two empty numpy arrays of data type float
delta_list_g = np.array([], dtype = float)
scale_list_g = np.array([], dtype = float)
delta_list_b = np.array([], dtype = float)
scale_list_b = np.array([], dtype = float)

#opens text file and appends data to both arrays
# with open('output_ug.txt', 'r') as csvfile:
#     reader = csv.reader(csvfile, delimiter=' ')
#     for row in reader:
#         delta_list = np.append(delta_list, row[0])
#         scale_list = np.append(scale_list, row[2])

print("new list")
f=open("output_figure4_green_rescale.txt", "r")
for line in f:
        if (line.find("#"))==-1:     #  Left this just as an example of how to remove comment lines!
            args=line.split()
            print(args[0],"  ",args[1])
            delta_list_g = np.append(delta_list_g,float(args[0]))
            scale_list_g = np.append(scale_list_g,float(args[1]))
f.close()
print("new list")
f=open("output_figure4_blue_rescale.txt", "r")
for line in f:
        if (line.find("#"))==-1:     #  Left this just as an example of how to remove comment lines!
            args=line.split()
            print(args[0],"  ",args[1])
            delta_list_b = np.append(delta_list_b,float(args[0]))
            scale_list_b = np.append(scale_list_b,float(args[1]))
f.close()



# creates an empty plot
plotFigure = pylab.figure()

# Code for creating a plot using all data
pylab.plot(delta_list_g, scale_list_g, color='g', label = 'qQ → gHqQ')
pylab.plot(delta_list_b, scale_list_b, color='b', label='qQ → gqHQ')
# adds labels and title to plot

plt.ylabel(r'$\frac{|M|^{2}}{s}$', fontsize=20)
plt.xlabel(r'$\Delta$', fontsize=20)
plt.legend()
plt.savefig('figure4_rescale.png', dpi=300, bbox_inches='tight')
#plots the graph
plt.show()


