#include <iostream>
#include <iomanip>

#include "CPPProcess.h"
#include "rambo.h"

int main(int argc, char** argv){

  // Create a process object
    CPPProcess process;

  // Read param_card and set parameters
    process.initProc("../../Cards/param_card.dat");

 // create a loop that increments over the value of Delta
    for(double Delta=0; Delta<10;Delta=Delta+0.01)
    {
        // defining relevant variables
        double ptxtot(0.), ptytot(0.), pplus(0.),pminus(0.);
        //  Need to define (n-1) 'pt's, (n-1) 'phi's and (n-1) 'y's
        double pt1=40.;
        double phi1=3.14159/3.;
        double y1=-Delta;   //pa and p1 are negative direction
        ptxtot+=pt1*cos(phi1);
        ptytot+=pt1*sin(phi1);
        pplus+=0.5*pt1*(cosh(y1)+sinh(y1));
        pminus+=0.5*pt1*(cosh(y1)-sinh(y1));
  //  Deduce 'n'th momentum by momentum conservation
        double ptn=sqrt(ptxtot*ptxtot+ptytot*ptytot);
        double yn=+Delta;
        pplus+=0.5*ptn*(cosh(yn)+sinh(yn));
        pminus+=0.5*ptn*(cosh(yn)-sinh(yn));
        // defining the value sij which is used to scale the matrix element
        double sij = 4*pplus*pminus;
        
  //  Use values above to find momenta
        vector<double*> p(1, new double[4]); //pa
        p[0][0] = pminus;
        p[0][1] = 0;
        p[0][2] = 0;
        p[0][3] = -pminus;
        p.push_back(new double[4]);  //pb
        p[1][0] = pplus;
        p[1][1] = 0;
        p[1][2] = 0;
        p[1][3] = pplus;
        p.push_back(new double[4]);  //p2
        p[2][0] = ptn*cosh(yn);
        p[2][1] = -ptxtot; // to balance px
        p[2][2] = -ptytot; // to balance py
        p[2][3] = ptn*sinh(yn);
        p.push_back(new double[4]);  //p1
        p[3][0] = pt1*cosh(y1);
        p[3][1] = pt1*cos(phi1);
        p[3][2] = pt1*sin(phi1);
        p[3][3] = pt1*sinh(y1);

        // checking if momentum is conserved
        if (p[0][0]+p[1][0]-p[2][0]-p[3][0] > 1.)
            {
                cout << "Error with momentum conservation!" << endl;
            }

  // Set momenta for this event
        process.setMomenta(p);

  // Evaluate matrix element
        process.sigmaKin();

        const double* matrix_elements = process.getMatrixElements();

        // outputs the value of Delta and the corresponding matrix element scaled
        for(int i=0; i<process.nprocesses;i++)
            {
                cout << Delta << "  " << matrix_elements[i]/(sij*sij)<< endl;
            }
    }
}
