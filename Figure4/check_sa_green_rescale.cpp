#include <iostream>
#include <iomanip>

#include "CPPProcess.h"
#include "rambo.h"

int main(int argc, char** argv){

  // Create a process object
    CPPProcess process;

  // Read param_card and set parameters
    process.initProc("../../Cards/param_card.dat");

    double energy = 1500;
    double weight;

  // Get phase space point
  //  vector<double*> p = get_momenta(process.ninitial, energy,
  //                 process.getMasses(), weight);
  // Copy the phase space routine from ../../src/rambo.cc
  // Choose parameters, Delta will become part of for loop eventually
    
    for(double Delta=0; Delta<10;Delta=Delta+0.01)
    {
        double ptxtot(0.), ptytot(0.), pplus(0.),pminus(0.);
  //  Need to define (n-1) 'pt's, (n-1) 'phi's and (n-1) 'y's
        double pplusj1jH(0.), pminusj1jH(0.);
        double pplusj2jH(0.), pminusj2jH(0.);
        double pplusj2j3(0.), pminusj2j3(0.);
        double pt1=40.;
        double pt3=40.;
        double pth=40.;
        double phi1=-3.14159/6.;
        double phi2= 3.14159/3;
        double phi3= -(7*3.14159)/9;
        double m=125.;
        double y1= -Delta;   //pa and p1 are negative direction
        double yh = -Delta/3;
        double y3 = Delta/3;
        double mperp = sqrt((m*m + pth*pth));
        ptxtot+=(pt1*cos(phi1)+pth*cos(phi2)+pt3*cos(phi3));
        ptytot+=(pt1*sin(phi1)+pth*sin(phi2)+pt3*sin(phi3));
        pplus+=(0.5*pt1*(cosh(y1)+sinh(y1))+0.5*mperp*(cosh(yh)+sinh(yh))+0.5*pt3*(cosh(y3)+sinh(y3)));
        pminus+=(0.5*pt1*(cosh(y1)-sinh(y1))+0.5*mperp*(cosh(yh)-sinh(yh))+0.5*pt3*(cosh(y3)-sinh(y3)));
        pplusj1jH+=(0.5*pt1*(cosh(y1)+sinh(y1))+0.5*mperp*(cosh(yh)+sinh(yh)));
        pminusj1jH+=(0.5*pt1*(cosh(y1)-sinh(y1))+0.5*mperp*(cosh(yh)+sinh(yh)));
        pplusj2jH+=(0.5*mperp*(cosh(yh)+sinh(yh))+0.5*pt3*(cosh(y3)+sinh(y3)));
        pminusj2jH+=(0.5*mperp*(cosh(yh)-sinh(yh))+0.5*pt3*(cosh(y3)-sinh(y3)));
  //  Deduce 'n'th momentum by momentum conservation
        double ptn=sqrt(ptxtot*ptxtot+ptytot*ptytot);
        double yn=+Delta;
        pplus+=0.5*ptn*(cosh(yn)+sinh(yn));
        pminus+=0.5*ptn*(cosh(yn)-sinh(yn));
        pplusj2j3+=(0.5*ptn*(cosh(yn)+sinh(yn))+0.5*pt3*(cosh(y3)+sinh(y3)));
        pminusj2j3+=(0.5*ptn*(cosh(yn)-sinh(yn))+0.5*pt3*(cosh(y3)-sinh(y3)));
        double sij = 4*pplus*pminus;

  //  Use values above to find momenta

        vector<double*> p(1, new double[4]); //pa, q
        p[0][0] = pminus;
        p[0][1] = 0;
        p[0][2] = 0;
        p[0][3] = -pminus;
        p.push_back(new double[4]);  //pb, Q
        p[1][0] = pplus;
        p[1][1] = 0;
        p[1][2] = 0;
        p[1][3] = pplus;
        p.push_back(new double[4]);  //p1, g
        p[2][0] = pt1*cosh(y1);
        p[2][1] = pt1*cos(phi1);
        p[2][2] = pt1*sin(phi1);
        p[2][3] = pt1*sinh(y1);
        p.push_back(new double[4]);  //p2, H
        p[3][0] = mperp*cosh(yh);
        p[3][1] = pth*cos(phi2);
        p[3][2] = pth*sin(phi2);
        p[3][3] = mperp*sinh(yh);
        p.push_back(new double[4]);  //p3, q
        p[4][0] = pt3*cosh(y3);
        p[4][1] = pt3*cos(phi3);
        p[4][2] = pt3*sin(phi3);
        p[4][3] = pt3*sinh(y3);
        p.push_back(new double[4]);  //p4, Q
        p[5][0] = ptn*cosh(yn);
        p[5][1] = -ptxtot; // to balance px
        p[5][2] = -ptytot; // to balance py
        p[5][3] = ptn*sinh(yn);
        
        
        
        
        double sj1jH = (m*m +2*((p[2][0]*p[3][0]) - (p[2][1]*p[3][1]) - (p[2][2]*p[3][2]) - (p[2][3]*p[3][3])));
        double sj2jH = (m*m +2*((p[4][0]*p[3][0]) - (p[4][1]*p[3][1]) - (p[4][2]*p[3][2]) - (p[4][3]*p[3][3])));
        double sj2j3 = (2*((p[4][0]*p[5][0]) - (p[4][1]*p[5][1]) - (p[4][2]*p[5][2]) - (p[4][3]*p[5][3])));
        

        if (p[0][0]+p[1][0]-p[2][0]-p[3][0] - p[4][0] -p[5][0] > 1.)
            {
                cout << "Error with momentum conservation!" << endl;
            }

  // Set momenta for this event
        process.setMomenta(p);

  // Evaluate matrix element
        process.sigmaKin();

        const double* matrix_elements = process.getMatrixElements();

 // cout << "Momenta:" << endl;
       // for(int i=0;i < process.nexternal; i++)
           // cout << setw(4) << i+1
           // << setiosflags(ios::scientific) << setw(14) << p[i][0]
           // << setiosflags(ios::scientific) << setw(14) << p[i][1]
          //  << setiosflags(ios::scientific) << setw(14) << p[i][2]
          //  << setiosflags(ios::scientific) << setw(14) << p[i][3] << endl;
 // cout << " -----------------------------------------------------------------------------" << endl;

  // Display matrix elements
       
//     << setiosflags(ios::fixed) << setprecision(17)
       for(int i=0; i<process.nprocesses;i++)
           {//cout << " Matrix element = "
  //     << setiosflags(ios::fixed) << setprecision(17)
                   // << matrix_elements[0]
                  //  << " GeV^" << -(2*process.nexternal-8) << endl;
                cout << Delta << "  " << matrix_elements[i]/(sj1jH*sj2jH*sj2j3*sj2j3)<< endl;
            }
    }
}
