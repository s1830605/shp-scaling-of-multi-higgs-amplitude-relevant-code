#include <iostream>
#include <iomanip>

#include "CPPProcess.h"
#include "rambo.h"

int main(int argc, char** argv){

  // Create a process object
    CPPProcess process;

  // Read param_card and set parameters
    process.initProc("../../Cards/param_card.dat");

    double energy = 1500;
    double weight;

  // Get phase space point
  //  vector<double*> p = get_momenta(process.ninitial, energy,
  //                 process.getMasses(), weight);
  // Copy the phase space routine from ../../src/rambo.cc
  // Choose parameters, Delta will become part of for loop eventually
    
    for(double Delta=0; Delta<10;Delta=Delta+0.01)
    {
        double ptxtot(0.), ptytot(0.), pplus(0.),pminus(0.);
  //  Need to define (n-1) 'pt's, (n-1) 'phi's and (n-1) 'y's
        double pplusjHj1(0.), pminusjHj1(0.);
        double pplusjH1jH2(0.), pminusjH1jH2(0.);
        double pth1=40.;
        double pth2 =40.;
        double phi1=3.14159/3.;
        double phi2= -(7*3.14159)/9;
        double m=125.;
        double yh1=Delta/3;   //pa and p1 are negative direction
        double yh2 = +Delta;
        double mperp = (sqrt(m*m + pth1*pth1));
        ptxtot+=(pth1*cos(phi1)+pth2*cos(phi2));
        ptytot+=(pth1*sin(phi1)+pth2*sin(phi2));
        pplus+=(0.5*mperp*(cosh(yh1)+sinh(yh1))+0.5*mperp*(cosh(yh2)+sinh(yh2)));
        pminus+=(0.5*mperp*(cosh(yh1)-sinh(yh1))+0.5*mperp*(cosh(yh2)-sinh(yh2)));
        
  //  Deduce 'n'th momentum by momentum conservation
        double ptn=sqrt(ptxtot*ptxtot+ptytot*ptytot);
        double yn=-Delta;
        pplus+=0.5*ptn*(cosh(yn)+sinh(yn));
        pminus+=0.5*ptn*(cosh(yn)-sinh(yn));
        double sij = 4*pplus*pminus;
        
        pplusjHj1+=(0.5*mperp*(cosh(yh1)+sinh(yh1))+0.5*ptn*(cosh(yn)+sinh(yn)));
        pminusjHj1+=(0.5*mperp*(cosh(yh1)-sinh(yh1))+0.5*ptn*(cosh(yn)+sinh(yn)));
        
        pplusjH1jH2+=(0.5*mperp*(cosh(yh1)+sinh(yh1))+0.5*mperp*(cosh(yh2)+sinh(yh2)));
        pminusjH1jH2+=(0.5*mperp*(cosh(yh1)-sinh(yh1))+0.5*mperp*(cosh(yh2)-sinh(yh2)));
        
  //  Use values above to find momenta

        vector<double*> p(1, new double[4]); //pa g
        p[0][0] = pminus;
        p[0][1] = 0;
        p[0][2] = 0;
        p[0][3] = -pminus;
        p.push_back(new double[4]);  //pb q
        p[1][0] = pplus;
        p[1][1] = 0;
        p[1][2] = 0;
        p[1][3] = pplus;
        p.push_back(new double[4]);  //p1 H
        p[2][0] = mperp*cosh(yh1);
        p[2][1] = pth1*cos(phi1);
        p[2][2] = pth1*sin(phi1);
        p[2][3] = mperp*sinh(yh1);
        p.push_back(new double[4]);  //p2 H
        p[3][0] = mperp*cosh(yh2);
        p[3][1] = pth2*cos(phi2);
        p[3][2] = pth2*sin(phi2);
        p[3][3] = mperp*sinh(yh2);
        p.push_back(new double[4]);  //p3 q
        p[4][0] = ptn*cosh(yn);
        p[4][1] = -ptxtot; // to balance px
        p[4][2] = -ptytot; // to balance py
        p[4][3] = ptn*sinh(yn);

        
        double sjHj1 = (m*m +2*((p[2][0]*p[4][0]) - (p[2][1]*p[4][1]) - (p[2][2]*p[4][2]) - (p[2][3]*p[4][3])));
        double sjH1jH2 = (m*m + m*m +2*((p[2][0]*p[3][0]) - (p[2][1]*p[3][1]) - (p[2][2]*p[3][2]) - (p[2][3]*p[3][3])));
        
        
        if (p[0][0]+p[1][0]-p[2][0]-p[3][0] -p[4][0] > 1.)
            {
                cout << "Error with momentum conservation!" << endl;
            }

  // Set momenta for this event
        process.setMomenta(p);

  // Evaluate matrix element
        process.sigmaKin();

        const double* matrix_elements = process.getMatrixElements();
        
 // cout << "Momenta:" << endl;
       // for(int i=0;i < process.nexternal; i++)
           // cout << setw(4) << i+1
           // << setiosflags(ios::scientific) << setw(14) << p[i][0]
           // << setiosflags(ios::scientific) << setw(14) << p[i][1]
          //  << setiosflags(ios::scientific) << setw(14) << p[i][2]
          //  << setiosflags(ios::scientific) << setw(14) << p[i][3] << endl;
 // cout << " -----------------------------------------------------------------------------" << endl;

  // Display matrix elements
       
//     << setiosflags(ios::fixed) << setprecision(17)
        for(int i=0; i<process.nprocesses;i++)
            {//cout << " Matrix element = "
                //     << setiosflags(ios::fixed) << setprecision(17)
                                 // << matrix_elements[0]
                                //  << " GeV^" << -(2*process.nexternal-8) << endl;
                cout << Delta << "  " << matrix_elements[i]/(sjHj1*sjH1jH2)<< endl;
                
            }
    }
}
