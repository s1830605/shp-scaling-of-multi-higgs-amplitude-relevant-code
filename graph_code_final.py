#import relevant libraries
import pylab
import numpy as np
import matplotlib.pyplot as plt

# creates two empty numpy arrays of data type float
delta_list = np.array([], dtype = float)
scale_list = np.array([], dtype = float)


# code that will open the text file assign the delta values to the delta list and the matrix element scaled to the scale list
f=open("output_multi.txt", "r")
for line in f:
    args=line.split()
    print(args[0],"  ",args[1])
    delta_list = np.append(delta_list,float(args[0]))
    scale_list = np.append(scale_list,float(args[1]))
f.close()


# creates an empty plot
plotFigure = pylab.figure()

# Code for creating a plot using all data
#pylab.plot(delta_list, scale_list, color='g')
pylab.plot(delta_list, scale_list)

# adds labels to plot
plt.ylabel(r'$\frac{|M|^{2}}{ s^{2}}$', fontsize=20)
#plt.ylabel(r'$\sqrt{s}$', fontsize=20)
#plt.ylabel(r'$|M^{2}|$', fontsize=20)
plt.xlabel(r'$\Delta$', fontsize=20)
plt.savefig('multi.png', dpi=300, bbox_inches='tight')
#plots the graph
pylab.show()
